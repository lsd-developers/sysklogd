struct sym_table
{
	unsigned long value;
	char *name;
};

struct Module
{
	struct sym_table *sym_array;
	int num_syms;

	char *name;
};

