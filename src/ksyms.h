struct symbol
{
	char *name;
	int size;
	int offset;
};


/* Function prototypes. */
extern char * LookupSymbol(unsigned long, struct symbol *);
extern char * LookupModuleSymbol(unsigned long int, struct symbol *);
