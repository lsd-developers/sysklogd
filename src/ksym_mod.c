/* Includes. */
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <ctype.h>
#include <malloc.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include "module.h"
#if !defined(__GLIBC__)
#include <linux/time.h>
#endif /* __GLIBC__ */
#include <paths.h>
#include <linux/version.h>

#include "klogd.h"
#include "ksyms.h"
#include "config.h"

static int num_modules = 0;
struct Module *sym_array_modules = (struct Module *) 0;

static int have_modules = 0;

/* Function prototypes. */
static void FreeModules(void);
static int AddSymbol(const char *);
struct Module *AddModule(const char *);
static int symsort(const void *, const void *);

/* Imported from ksym.c */
extern int num_syms;

/**************************************************************************
 * Function:	InitMsyms
 *
 * Purpose:	This function is responsible for building a symbol
 *		table which can be used to resolve addresses for
 *		loadable modules.
 *
 * Arguments:	Void
 *
 * Return:	A boolean return value is assumed.
 *
 *		A false value indicates that something went wrong.
 *
 *		True if loading is successful.
 **************************************************************************/

extern int InitMsyms()

{
	auto int	rtn,
			tmp;

	FILE *ksyms;

	char buf[128];
	char *p;

	/* Initialize the kernel module symbol table. */
	FreeModules();

	ksyms = fopen(_PATH_KSYMS, "r");

	if ( ksyms == NULL )
	{
		if ( errno == ENOENT )
			Syslog(LOG_INFO, "No module symbols loaded - "
			       "kernel modules not enabled.\n");
		else
			Syslog(LOG_ERR, "Error loading kernel symbols " \
			       "- %s\n", strerror(errno));
		return(0);
	}

	while ( fgets(buf, sizeof(buf), ksyms) != NULL )
	{
		if (num_syms > 0 && index(buf, '[') == NULL)
			continue;

		p = index(buf, ' ');

		if ( p == NULL )
			continue;

		if ( buf[strlen(buf)-1] == '\n' )
			buf[strlen(buf)-1] = '\0';
		/* overlong lines will be ignored above */

		AddSymbol(buf);
	}

	fclose(ksyms);

	have_modules = 1;

	/* Sort the symbol tables in each module. */
	for (rtn = tmp = 0; tmp < num_modules; ++tmp)
	{
		rtn += sym_array_modules[tmp].num_syms;
		if ( sym_array_modules[tmp].num_syms < 2 )
			continue;
		qsort(sym_array_modules[tmp].sym_array, \
		      sym_array_modules[tmp].num_syms, \
		      sizeof(struct sym_table), symsort);
	}

	if ( rtn == 0 )
		Syslog(LOG_INFO, "No module symbols loaded.");
	else
		Syslog(LOG_INFO, "Loaded %d %s from %d module%s", rtn, \
		       (rtn == 1) ? "symbol" : "symbols", \
		       num_modules, (num_modules == 1) ? "." : "s.");

	return(1);
}


static int symsort(p1, p2)

     const void *p1;

     const void *p2;

{
	auto const struct sym_table	*sym1 = p1,
					*sym2 = p2;

	if ( sym1->value < sym2->value )
		return(-1);
	if ( sym1->value == sym2->value )
		return(0);
	return(1);
}


/**************************************************************************
 * Function:	FreeModules
 *
 * Purpose:	This function is used to free all memory which has been
 *		allocated for the modules and their symbols.
 *
 * Arguments:	None specified.
 *
 * Return:	void
 **************************************************************************/

static void FreeModules()

{
	auto int	nmods,
			nsyms;

	auto struct Module *mp;


	/* Check to see if the module symbol tables need to be cleared. */
	have_modules = 0;
	if ( num_modules == 0 )
		return;

	if ( sym_array_modules == NULL )
		return;

	for (nmods = 0; nmods < num_modules; ++nmods)
	{
		mp = &sym_array_modules[nmods];
		if ( mp->num_syms == 0 )
			continue;
	       
		for (nsyms= 0; nsyms < mp->num_syms; ++nsyms)
			free(mp->sym_array[nsyms].name);
		free(mp->sym_array);
		if ( mp->name != NULL )
			free(mp->name);
	}

	free(sym_array_modules);
	sym_array_modules = (struct Module *) 0;
	num_modules = 0;
	return;
}


/**************************************************************************
 * Function:	AddModule
 *
 * Purpose:	This function is responsible for adding a module to
 *		the list of currently loaded modules.
 *
 * Arguments:	(const char *) module
 *
 *		module:->	The name of the module.
 *
 * Return:	struct Module *
 **************************************************************************/

struct Module *AddModule(module)

     const char *module;

{
	struct Module *mp;

	if ( num_modules == 0 )
	{
		sym_array_modules = (struct Module *)malloc(sizeof(struct Module));

		if ( sym_array_modules == NULL )
		{
			Syslog(LOG_WARNING, "Cannot allocate Module array.\n");
			return NULL;
		}
		mp = sym_array_modules;
	}
	else
	{
		/* Allocate space for the module. */
		mp = (struct Module *) \
			realloc(sym_array_modules, \
				(num_modules+1) * sizeof(struct Module));

		if ( mp == NULL )
		{
			Syslog(LOG_WARNING, "Cannot allocate Module array.\n");
			return NULL;
		}

		sym_array_modules = mp;
		mp = &sym_array_modules[num_modules];
	}

	num_modules++;
	mp->sym_array = NULL;
	mp->num_syms = 0;

	if ( module != NULL )
		mp->name = strdup(module);
	else
		mp->name = NULL;

	return mp;
}


/**************************************************************************
 * Function:	AddSymbol
 *
 * Purpose:	This function is responsible for adding a symbol name
 *		and its address to the symbol table.
 *
 * Arguments:	(struct Module *) mp, (unsigned long) address, (char *) symbol
 *
 *		mp:->	A pointer to the module which the symbol is
 *			to be added to.
 *
 *		address:->	The address of the symbol.
 *
 *		symbol:->	The name of the symbol.
 *
 * Return:	int
 *
 *		A boolean value is assumed.  True if the addition is
 *		successful.  False if not.
 **************************************************************************/

static int AddSymbol(line)

	const char *line;
	
{
	char *module;
	unsigned long address;
	char *p;
	static char *lastmodule = NULL;
	struct Module *mp;

	module = index(line, '[');

	if ( module != NULL )
	{
		p = index(module, ']');

		if ( p != NULL )
			*p = '\0';

		p = module++;

		while ( isspace(*(--p)) );
		*(++p) = '\0';
	}

	p = index(line, ' ');

	if ( p == NULL )
		return(0);

	*p = '\0';

	address  = strtoul(line, (char **) 0, 16);

	p += 3;

	if ( num_modules == 0 ||
	     ( lastmodule == NULL && module != NULL ) ||
	     ( module == NULL && lastmodule != NULL) ||
	     ( module != NULL && strcmp(module, lastmodule)))
	{
		mp = AddModule(module);

		if ( mp == NULL )
			return(0);
	}
	else
		mp = &sym_array_modules[num_modules-1];

	lastmodule = mp->name;

	/* Allocate space for the symbol table entry. */
	mp->sym_array = (struct sym_table *) realloc(mp->sym_array, \
        	(mp->num_syms+1) * sizeof(struct sym_table));

	if ( mp->sym_array == (struct sym_table *) 0 )
		return(0);

	mp->sym_array[mp->num_syms].name = strdup(p);
	if ( mp->sym_array[mp->num_syms].name == (char *) 0 )
		return(0);
	
	/* Stuff interesting information into the module. */
	mp->sym_array[mp->num_syms].value = address;
	++mp->num_syms;

	return(1);
}


/**************************************************************************
 * Function:	LookupModuleSymbol
 *
 * Purpose:	Find the symbol which is related to the given address from
 *		a kernel module.
 *
 * Arguments:	(long int) value, (struct symbol *) sym
 *
 *		value:->	The address to be located.
 * 
 *		sym:->		A pointer to a structure which will be
 *				loaded with the symbol's parameters.
 *
 * Return:	(char *)
 *
 *		If a match cannot be found a diagnostic string is printed.
 *		If a match is found the pointer to the symbolic name most
 *		closely matching the address is returned.
 **************************************************************************/

extern char * LookupModuleSymbol(value, sym)

	unsigned long value;

	struct symbol *sym;
	
{
	auto int	nmod,
			nsym;

	auto struct sym_table *last;

	auto struct Module *mp;

	static char ret[100];


	sym->size = 0;
	sym->offset = 0;
	if ( num_modules == 0 )
		return((char *) 0);
	
	for (nmod = 0; nmod < num_modules; ++nmod)
	{
		mp = &sym_array_modules[nmod];

		/*
		 * Run through the list of symbols in this module and
		 * see if the address can be resolved.
		 */
		for(nsym = 1, last = &mp->sym_array[0];
		    nsym < mp->num_syms;
		    ++nsym)
		{
			if ( mp->sym_array[nsym].value > value )
			{
			    if ( sym->size == 0 ||
				 (value - last->value) < sym->offset ||
				 ( (sym->offset == (value - last->value)) &&
				   (mp->sym_array[nsym].value-last->value) < sym->size ) )
			    {
				sym->offset = value - last->value;
				sym->size = mp->sym_array[nsym].value - \
					last->value;
				ret[sizeof(ret)-1] = '\0';
				if ( mp->name == NULL )
					snprintf(ret, sizeof(ret)-1,
						 "%s", last->name);
				else
					snprintf(ret, sizeof(ret)-1,
						 "%s:%s", mp->name, last->name);
			    }
			    break;
			}
			last = &mp->sym_array[nsym];
		}
	}

	if ( sym->size > 0 )
		return(ret);

	/* It has been a hopeless exercise. */
	return((char *) 0);
}

