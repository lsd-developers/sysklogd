#ifndef CONFIG_H
#define CONFIG_H

#define VERSION "1.6.3"

#ifndef _PATH_UTMP
#define _PATH_UTMP _PATH_VARRUN "utmp"
#endif

#define _PATH_LOGCONF   "/etc/syslog.conf"
#define _PATH_SYSLOGPID _PATH_VARRUN "syslogd.pid"
#define _PATH_KLOGPID _PATH_VARRUN "klogd.pid"
#define _PATH_DEV       "/dev/"
#define _PATH_CONSOLE   "/dev/console"
#define _PATH_TTY       "/dev/tty"
#define _PATH_LOG       "/dev/log"
#define _PATH_KSYMS     "/proc/kallsyms"

#define LOG_BUFFER_SIZE 4096
#define LOG_LINE_LENGTH 1000

#endif /* CONFIG_H */