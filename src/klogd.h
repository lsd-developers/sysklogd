/* Useful include files. */
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <stdarg.h>

/* Function prototypes. */
extern int InitKsyms(char *);
extern int InitMsyms(void);
extern char * ExpandKadds(char *, char *);
extern void SetParanoiaLevel(int);
extern void Syslog(int priority, char *fmt, ...);
extern void syslog_own(int, const char *, ...);
extern void vsyslog_own(int, const char *, va_list);
