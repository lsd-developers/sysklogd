all install clean:
	$(MAKE) -C src $@
	$(MAKE) -C man $@
	$(MAKE) -C vim $@

dist:
	git archive HEAD --prefix=sysklogd-1.6.3/ | xz > sysklogd-1.6.3.tar.xz